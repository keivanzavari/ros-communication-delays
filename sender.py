#! /usr/bin/env python3
import rospy
import sv_msgs.msg
import sv_msgs.srv


class Sender:
    def __init__(self) -> None:
        rospy.init_node('sender', log_level=rospy.WARN)
        self.msg_to_send = sv_msgs.msg.ValueWithTime()
        self.pub = rospy.Publisher('/sender/data', sv_msgs.msg.ValueWithTime, latch=True, queue_size=1)
        self.srv = rospy.Service("/sender/srv", sv_msgs.srv.setValueWithTime, self.srv_callback)
        # self.pers_srv = rospy.Service("/sender/persistent_srv", sv_msgs.srv.setValueWithTime, self.pers_srv_callback)
        self.timer = rospy.Timer(rospy.Duration.from_sec(0.1), self.publish)

        # rospy.Subscriber('/computer_vision/reid/hotzone', std_msgs.msg.Bool, self.callback_hotzone, queue_size=1)

    def srv_callback(self, req: sv_msgs.srv.setValueWithTimeRequest) -> sv_msgs.srv.setValueWithTimeResponse:
        # print("service call")
        return sv_msgs.srv.setValueWithTimeResponse(timestamp=rospy.Time.now())

    # def pers_srv_callback(self, req: sv_msgs.srv.setValueWithTimeRequest) -> sv_msgs.srv.setValueWithTimeResponse:
    #     # print("service call")
    #     return sv_msgs.srv.setValueWithTimeResponse(timestamp=rospy.Time.now())

    def publish(self, _) -> None:
        self.msg_to_send.value = 0
        self.msg_to_send.timestamp = rospy.Time.now()
        self.pub.publish(self.msg_to_send)


if __name__ == '__main__':

    try:
        sender = Sender()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
