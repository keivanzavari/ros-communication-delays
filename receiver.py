#! /usr/bin/env python3
from typing import Dict, List
import matplotlib.pyplot as plt
import numpy as np

import rospy
import sv_msgs.msg
import sv_msgs.srv


class Receiver:
    def __init__(self) -> None:
        rospy.init_node('receiver', log_level=rospy.WARN)
        self.collected_data = {
            "pub": {
                "send_time": [],
                "recv_time": []
            },
            "srv": {
                "send_time": [],
                "recv_time": [],
                "server_recv_time": []
            }
        }

        self.srv = rospy.ServiceProxy("/sender/srv", sv_msgs.srv.setValueWithTime)
        self.sub = rospy.Subscriber('/sender/data', sv_msgs.msg.ValueWithTime, self.callback, queue_size=1)
        self.timer = rospy.Timer(rospy.Duration.from_sec(0.1), self.call_srv)

    def call_srv(self, _):
        req = sv_msgs.srv.setValueWithTimeRequest()
        req.req.value = 0
        req.req.timestamp = rospy.Time.now()
        res: sv_msgs.srv.setValueWithTimeResponse = self.srv(req)
        recv_time = rospy.Time.now()

        self.collected_data["srv"]["send_time"].append(req.req.timestamp.to_sec())
        self.collected_data["srv"]["recv_time"].append(recv_time.to_sec())
        self.collected_data["srv"]["server_recv_time"].append(res.timestamp.to_sec())

    def callback(self, msg: sv_msgs.msg.ValueWithTime) -> None:
        recv_time = rospy.Time.now()
        self.collected_data["pub"]["send_time"].append(msg.timestamp.to_sec())
        self.collected_data["pub"]["recv_time"].append(recv_time.to_sec())


def calculate_diff(data: Dict[str, Dict[str, List[float]]]) -> Dict[str, np.ndarray]:
    # data = {"pub": {"send_time": [], "recv_time": []}, "srv": {"send_time": [], "recv_time": []}}
    send = np.array(data["pub"]["send_time"])
    recv = np.array(data["pub"]["recv_time"])
    pub_diff = (recv - send) * 1000
    send = np.array(data["srv"]["send_time"])
    recv = np.array(data["srv"]["server_recv_time"])
    srv_diff = (recv - send) * 1000

    # recv = np.array(data["srv"]["server_recv_time"])
    # server_srv_diff = (recv - send) * 1000

    return {"pub_diff": pub_diff[5:], "srv_diff": srv_diff[5:]}


def plot(data: Dict[str, np.ndarray]) -> None:
    fig, ax = plt.subplots(2, 1, sharex=True, figsize=(16, 9))
    ax[0].plot(data["pub_diff"], "o--r", label="publish, subscribe delay in msec, round")
    ax[0].set_ylabel("Publisher subscriber delay [ms]")
    ax[0].set_title("delay comparison of service calls & publisher subscribers")
    ax[0].legend()
    ax[0].grid(True)

    ax[1].plot(data["srv_diff"], "+-.", color="navy", label="service delay in msec, send until server recv")
    ax[1].set_ylabel("service delay [ms]")
    ax[1].legend()
    ax[1].grid(True)

    # ax[3].plot(data["server_srv_diff"], "*-.", label="service delay in msec, send until server recv")
    # ax[3].set_ylabel("service delay [ms]")
    # ax[3].legend()
    # ax[3].grid(True)

    fig.savefig("/home/sv/bags/figure.png", dpi=300)


if __name__ == '__main__':

    receiver = Receiver()
    try:
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
    len1 = len(receiver.collected_data["pub"]["send_time"])
    len2 = len(receiver.collected_data["srv"]["send_time"])
    print(f"collected {len1}, {len2} samples")
    diff = calculate_diff(receiver.collected_data)
    plot(diff)
